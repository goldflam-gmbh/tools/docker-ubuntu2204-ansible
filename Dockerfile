FROM ubuntu:22.04

# Build arguments
ARG ansible_version

ARG DEBIAN_FRONTEND=noninteractive

# Install Ansible dependencies
# - netaddr       for Ansible filters 'ipaddr()', 'ipsubnet()', etc. Docs: https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters_ipaddr.html
# - dnspython     for Ansible 'dig' lookup plugin to look up DNS entries of a domain. Docs: https://docs.ansible.com/ansible/latest/collections/community/general/dig_lookup.html
# - pykeepass     for Ansible 'keepass' lookup plugin to access Keepass vaults (needs 3.2.x releases, see  https://github.com/viczem/ansible-keepass/commit/a413bf284e588b74e1101e3d2a8d74f351c3306a)
# - passlib       for Ansible filter 'password_hash()'. Docs: https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#encrypting-and-checksumming-strings-and-passwords
# - bcrypt        for Ansible filter 'password_hash()'.
# - hcloud        for accessing a dynamic Ansible inventory at Hetzner Cloud. Repo: https://github.com/hetznercloud/cli
# - ionoscloud    for accessing a dynamic Ansible inventory at Ionos Cloud. Docs: https://docs.ionos.com/ansible/
ENV pip_packages "netaddr dnspython pykeepass==4.0.3 passlib bcrypt hcloud ionoscloud"

# Install dependencies.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       apt-utils \
       build-essential \
       openssh-client \
       openjdk-11-jre-headless \
       maven \
       locales \
       libffi-dev \
       libssl-dev \
       libyaml-dev \
       python3-dev \
       python3-setuptools \
       python3-pip \
       python3-yaml \
       software-properties-common \
       rsyslog systemd systemd-cron sudo iproute2 \
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man
RUN sed -i 's/^\($ModLoad imklog\)/#\1/' /etc/rsyslog.conf

# Fix potential UTF-8 errors with ansible-test.
RUN locale-gen en_US.UTF-8

# Install Ansible and other Packages via Pip.
RUN pip3 install ansible==$ansible_version $pip_packages

COPY initctl_faker .
RUN chmod +x initctl_faker && rm -fr /sbin/initctl && ln -s /initctl_faker /sbin/initctl

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts

# Remove unnecessary getty and udev targets that result in high CPU usage when using
# multiple containers with Molecule (https://github.com/ansible/molecule/issues/1104)
RUN rm -f /lib/systemd/system/systemd*udev* \
  && rm -f /lib/systemd/system/getty.target

WORKDIR /data

VOLUME ["/sys/fs/cgroup", "/tmp", "/run", "/data"]

CMD ["/lib/systemd/systemd"]
